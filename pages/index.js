import { useContext } from 'react'
import { Box, Flex } from 'reflexbox'

import AppContext from 'contexts/AppContext'
import Navigation from 'components/Navigation'
import SearchForm from 'components/SearchForm'
import Contact from 'components/Contact'
import AddContact from 'components/AddContact'

import layoutTheme from 'theme/layout-theme'

const navigation = [
    {
        label: 'All contacts',
        url: '/'
    },
    {
        label: 'My favorites',
        url: '/favorites'
    }
]

const Index = () => {
    const { contacts } = useContext(AppContext)

    return (
        <div>
            <Box theme={layoutTheme} variant="container1680" px={25} mb={[20, null, null, 40, 60, 80]}>
                <Navigation items={navigation} />
            </Box>

            <Box theme={layoutTheme} variant="container1260" px={25} mb={[25, null, null, 40, 60, 80]}>
                <SearchForm />
            </Box>

            <Flex
                theme={layoutTheme}
                variant="container1560"
                px={10}
                mb={100}
                flexDirection={['column', null, 'row']}
                flexWrap="wrap">
                <AddContact />
                {contacts.map(item => (
                    <Contact key={item.id} contact={item} />
                ))}
            </Flex>
        </div>
    )
}

export default Index
