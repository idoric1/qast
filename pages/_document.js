import Document, { Html, Head, Main, NextScript } from 'next/document'
import AppIcons from 'components/AppIcons'

const { GTM, IS_DEV } = process.env

class TypeqastDocument extends Document {
    render() {
        /* eslint-disable no-underscore-dangle, react/no-danger */
        const { lng } = this.props
        return (
            <Html lang={lng}>
                <Head>
                    <meta charSet="utf-8" key="charset" />
                    {/* Use minimum-scale=1 to enable GPU rasterization */}
                    <meta
                        name="viewport"
                        content="minimum-scale=1,
                                initial-scale=1,
                                width=device-width,
                                shrink-to-fit=no"
                        key="viewport"
                    />

                    {AppIcons()}

                    <link
                        href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap"
                        rel="stylesheet"
                    />

                    {!IS_DEV && GTM && (
                        <script
                            dangerouslySetInnerHTML={{
                                __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                            })(window,document,'script','dataLayer','${GTM}');`
                            }}
                        />
                    )}
                    <script
                        dangerouslySetInnerHTML={{
                            __html: 'if (!window.dataLayer) window.dataLayer = [];'
                        }}
                    />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default TypeqastDocument
