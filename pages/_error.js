import PropTypes from 'prop-types'

const Error = ({ statusCode }) => <div>Error</div>

Error.getInitialProps = async ({ res, err }) => {
    let statusCode
    if (res) ({ statusCode } = res)
    else if (err) ({ statusCode } = err)
    else statusCode = null

    return {
        statusCode
    }
}

Error.propTypes = {
    statusCode: PropTypes.number
}

Error.defaultProps = {
    statusCode: undefined
}

export default Error
