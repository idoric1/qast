import DataDisplay from 'components/DataDisplay'
import ContactHeader from 'components/ContactHeader'
import { useRouter } from 'next/router'
import AppContext from 'contexts/AppContext'
import { useContext, useEffect, useState } from 'react'

const View = () => {
    const { contacts } = useContext(AppContext)
    const router = useRouter()
    const contact = contacts.length !== 0 ? contacts.find(item => item.id === +router.query.id) : null

    return (
        <div>
            <ContactHeader
                variant="view"
                fullName={contact.fullName}
                imageUrl={contact.imageUrl}
                contactId={contact.id}
                isFavorite={contact.favorite}
            />
            <DataDisplay variant="email" email={contact.email} />
            <DataDisplay variant="numbers" numbers={contact.numbers} />
        </div>
    )
}

export default View
