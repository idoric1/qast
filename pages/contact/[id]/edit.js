import { useContext, useEffect } from 'react'
import { useRouter } from 'next/router'
import AddEditForm from 'components/AddEditForm'
import AppContext from 'contexts/AppContext'

const Add = () => {
    const { contactToBeDeleted, setContactToBeDeleted, contacts, setContacts } = useContext(AppContext)
    const router = useRouter()
    const contact = contacts.length !== 0 ? contacts.find(item => item.id === +router.query.id) : null

    // Delete logic for deleting contact when on contact page
    // Doing it here so that we can delete it when component unmounts
    useEffect(() => {
        return function deleteContact() {
            if (contactToBeDeleted) {
                const filtered = [...contacts].filter(item => {
                    return item.id !== contactToBeDeleted
                })

                localStorage.setItem('contacts', JSON.stringify(filtered))
                setContacts(filtered)
                setContactToBeDeleted(null)
            }
        }
    })

    return <AddEditForm contact={contact} />
}

export default Add
