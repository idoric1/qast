import App from 'next/app'
import { ThemeProvider } from 'emotion-theming'
import theme from 'theme'
import BaseLayout from 'layouts/Base'
import NextNprogress from 'nextjs-progressbar'

import GlobalStyles from 'components/GlobalStyles'
import Header from 'components/Header'
import dynamic from 'next/dynamic'

// Had to call it dynamically to have accessto localStorage
const ContextWrapper = dynamic(() => import('components/ContextWrapper'), {
    ssr: false
})

// eslint-disable-next-line react/prop-types
function TypeqastApp({ Component }) {
    return (
        <>
            <ThemeProvider theme={theme.typeqast}>
                <NextNprogress height="5" color="#C26464" options={{ easing: 'ease', speed: 500 }} />
                <GlobalStyles />
                <ContextWrapper>
                    <BaseLayout>
                        <Header />
                        <Component />
                    </BaseLayout>
                </ContextWrapper>
            </ThemeProvider>
        </>
    )
}

export default TypeqastApp
