const contacts = [
    {
        id: 1,
        fullName: 'Addie Hernandez',
        email: 'addie.hernandez@gmail.com',
        imageUrl: '/images/contacts/addie-hernandez.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Work',
                number: '+385 21 147 258'
            },
            {
                id: 3,
                label: 'Cell',
                number: '+385 98 6547 541'
            },
            {
                id: 4,
                label: 'Husband',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 2,
        fullName: 'Oscar Arnold',
        email: 'oscar.arnold@gmail.com',
        imageUrl: '/images/contacts/oscar-arnold.jpg',
        favorite: true,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Work',
                number: '+385 21 147 258'
            },
            {
                id: 3,
                label: 'Cell',
                number: '+385 98 6547 541'
            }
        ]
    },
    {
        id: 3,
        fullName: 'Isaiah McGuire',
        email: 'isiah.mcguire@gmail.com',
        imageUrl: '/images/contacts/isaiah-mcguire.jpg',
        favorite: true,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Work',
                number: '+385 21 147 258'
            },
            {
                id: 3,
                label: 'Cell',
                number: '+385 98 6547 541'
            },
            {
                id: 4,
                label: 'Wife',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 4,
        fullName: 'Ann Schneider',
        email: 'ann.schneider@gmail.com',
        imageUrl: '/images/contacts/ann-schneider.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Husband',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 5,
        fullName: 'Agnes Terry',
        email: 'agnes.terry@gmail.com',
        imageUrl: '/images/contacts/agnes-terry.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Cell',
                number: '+385 98 6547 541'
            }
        ]
    },
    {
        id: 6,
        fullName: 'Rose Bush',
        email: 'rose.bush@gmail.com',
        imageUrl: '/images/contacts/rose-bush.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Cell',
                number: '+385 98 6547 541'
            },
            {
                id: 2,
                label: 'Husband',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 7,
        fullName: 'Duane Reese',
        email: 'duane.reese@gmail.com',
        imageUrl: '/images/contacts/duane-reese.jpg',
        favorite: true,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Work',
                number: '+385 21 147 258'
            }
        ]
    },
    {
        id: 8,
        fullName: 'Mae Chandler',
        email: 'mae.chandler@gmail.com',
        imageUrl: '/images/contacts/mae-chandler.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            }
        ]
    },
    {
        id: 9,
        fullName: 'Evelyn Weaver',
        email: 'evelyn.weaver@gmail.com',
        imageUrl: '/images/contacts/evelyn-weaver.jpg',
        favorite: true,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Home',
                number: '+385 21 546 456'
            },
            {
                id: 2,
                label: 'Husband',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 10,
        fullName: 'Catherine Moore',
        email: 'catherine.moore@gmail.com',
        imageUrl: '/images/contacts/catherine-moore.jpg',
        favorite: true,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Cell',
                number: '+385 98 6547 541'
            },
            {
                id: 2,
                label: 'Husband',
                number: '+385 99 1456 214'
            }
        ]
    },
    {
        id: 11,
        fullName: 'Sam Manning',
        email: 'sam.manning@gmail.com',
        imageUrl: '/images/contacts/sam-manning.jpg',
        favorite: false,
        visible: true,
        numbers: [
            {
                id: 1,
                label: 'Work',
                number: '+385 21 147 258'
            },
            {
                id: 2,
                label: 'Cell',
                number: '+385 98 6547 541'
            }
        ]
    }
]

export default contacts
