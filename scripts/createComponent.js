/* eslint-disable no-console */

const inquirer = require('inquirer')
const fs = require('fs')
const path = require('path')
const replaceString = require('replace-string')
const makeDir = require('make-dir')

const componentTypes = ['Screen', 'Component']
const suffixedComponentTypes = ['Screen']

const isSuffixed = type => suffixedComponentTypes.indexOf(type) > -1

const readFileAsString = relativeFilePath => {
    return fs.readFileSync(path.join(__dirname, relativeFilePath), 'utf-8')
}

const injectValuesIntoTemplate = (src, values) => {
    let result = src

    Object.keys(values).forEach(key => {
        result = replaceString(result, `%${key}%`, values[key])
    })

    return result
}

const questions = [
    {
        type: 'input',
        name: 'componentName',
        message: `Name of the component. (Must not contain words: ${componentTypes.join(', ')}):`,
        validate(input) {
            return (
                input.length &&
                input[0] === input[0].toUpperCase() &&
                componentTypes.every(type => input.indexOf(type) === -1)
            )
        }
    },
    {
        type: 'input',
        name: 'componentPath',
        message: 'Relative path of the component (from project root):',
        default: 'components'
    }
]

inquirer.prompt(questions).then(async answers => {
    const { componentPath, componentName: dirName } = answers
    const componentName = `${dirName}`
    const filesToRead = [
        '../templates/index.js.template',
        '../templates/style.js.template',
        `../templates/Component.js.template`
    ]
    const filesToWrite = ['index.js', `${componentName}.style.js`, `${componentName}.component.js`]
    const dirPath = path.join(__dirname, '../', componentPath, dirName)

    console.log('Generating...')

    const transformedSources = filesToRead.map(filePath => {
        const src = readFileAsString(filePath)
        return injectValuesIntoTemplate(src, {
            componentName,
            componentPath,
            dirName
        })
    })

    // create component directory
    makeDir.sync(dirPath)

    // write component files
    filesToWrite.forEach((relativeFilePath, index) => {
        const srcToWrite = transformedSources[index]
        fs.writeFileSync(path.join(dirPath, relativeFilePath), srcToWrite)
    })

    console.log(`Component created: ${dirPath.replace(path.join(__dirname, '../'), '')}`)
})
