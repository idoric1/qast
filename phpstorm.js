/* eslint-disable no-undef */
// This configuration is only used for PHPStorm to be able to resolve babel-resolver paths
System.config({
    paths: {
        'components/*': './components',
        'controllers/*': './controllers',
        'helpers/*': './helpers',
        'hooks/*': './hooks',
        'layouts/*': './layouts',
        'public/*': './public',
        'server/*': './server',
        'theme/*': './theme'
    }
})
