require('dotenv').config()
const path = require('path')
const DotenvWebpack = require('dotenv-webpack')

module.exports = {
    // Available on both server and client
    env: {
        // Use this to populate API variables and such
        DOMAIN_ROOT: process.env.DOMAIN_ROOT,
        GTM: process.env.GTM,
        DEFAULT_THEME: process.env.DEFAULT_THEME,
        API_URL: process.env.API_URL,
        SITE_URL: process.env.SITE_URL,
        IS_DEV: process.env.NODE_ENV !== 'production',
        CLOUDINARY_URL: process.env.CLOUDINARY_URL
    },
    webpack: config => {
        config.plugins = config.plugins || []

        config.plugins = [
            ...config.plugins,

            // Read the .env file
            new DotenvWebpack({
                path: path.join(__dirname, '.env'),
                systemvars: true
            })
        ]

        config.module.rules.push({
            test: /\.svg$/,
            use: ['@svgr/webpack']
        })

        config.module.rules.push({
            test: /\.md$/,
            use: 'raw-loader'
        })

        /* eslint-disable dot-notation */
        config.resolve.alias['components'] = path.join(__dirname, 'components')
        config.resolve.alias['controllers'] = path.join(__dirname, 'controllers')
        config.resolve.alias['helpers'] = path.join(__dirname, 'helpers')
        config.resolve.alias['contexts'] = path.join(__dirname, 'contexts')
        config.resolve.alias['hooks'] = path.join(__dirname, 'hooks')
        config.resolve.alias['layouts'] = path.join(__dirname, 'layouts')
        config.resolve.alias['public'] = path.join(__dirname, 'public')
        config.resolve.alias['theme'] = path.join(__dirname, 'theme')

        return config
    }
}
