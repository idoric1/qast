const style = ({ theme }) => ({
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    zIndex: 1,
    background: theme.colors.blueDark,
    color: theme.colors.grayDark,
    transition: 'all 0.5s ease'
})

export default style
