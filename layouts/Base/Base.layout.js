/** @jsx jsx */
import { jsx } from '@emotion/core'
import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'

import style from './Base.style'

const BaseLayoutStyled = styled.main(props => ({ ...style(props) }))

const BaseLayout = ({ children }) => {
    return <BaseLayoutStyled>{children}</BaseLayoutStyled>
}

BaseLayout.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
}

export default withTheme(BaseLayout)
