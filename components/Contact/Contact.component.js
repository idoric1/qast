import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import Link from 'next/link'
import ContactContextWrapper from 'components/ContactContextWrapper'

import FavoriteButton from 'components/FavoriteButton'
import DeleteButton from 'components/DeleteButton'
import EditIcon from 'public/icons/edit.svg'
import ConfirmationDialog from 'components/ConfirmationDialog'

import style from './Contact.style'

const ContactStyled = styled.div(props => ({ ...style(props) }))

const Contact = ({ contact }) => {
    return (
        <ContactStyled contactImage={contact.imageUrl}>
            <ContactContextWrapper>
                <Link href="contact/[id]/view" as={`/contact/${contact.id}/view`}>
                    <a className="blockLink">View contact details</a>
                </Link>

                <div className="image-name">
                    <div className="image" />
                    <div className="name">{contact.fullName}</div>
                </div>

                <div className="controls">
                    <FavoriteButton id={contact.id} isFavorite={contact.favorite} />
                    <Link href="/contact/[id]/edit" as={`/contact/${contact.id}/edit`}>
                        <a className="edit">
                            <EditIcon />
                        </a>
                    </Link>
                    <div className="delete">
                        <DeleteButton id={contact.id} />
                    </div>
                </div>

                <ConfirmationDialog
                    title="Delete"
                    text="Are you sure you want to delete this contact?"
                    confirmButtonText="Delete"
                />
            </ContactContextWrapper>
        </ContactStyled>
    )
}

Contact.propTypes = {
    contact: PropTypes.PropTypes.shape({
        id: PropTypes.number.isRequired,
        fullName: PropTypes.string.isRequired,
        imageUrl: PropTypes.string,
        favorite: PropTypes.bool.isRequired
    }).isRequired
}

export default withTheme(Contact)
