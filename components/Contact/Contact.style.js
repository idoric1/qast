import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, contactImage }) => ({
    padding: toRems([10, 15]),
    border: `1px solid ${theme.colors.greyLight}`,
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    marginBottom: toRem(10),

    [theme.breakpoints.up(580)]: {
        width: 'calc(50% - 10px)',
        marginRight: 20,
        flexDirection: 'column',
        padding: toRems([15, 15, 20, 15]),
        marginBottom: toRem(20),

        '&:nth-of-type(2n)': {
            marginRight: 0
        }
    },

    [theme.breakpoints.up('md')]: {
        width: 'calc(33.3333% - 13.3333px)',
        marginRight: 19.8,

        '&:nth-of-type(2n)': {
            marginRight: 19.8
        },

        '&:nth-of-type(3n)': {
            marginRight: 0
        }
    },

    [theme.breakpoints.up('lg')]: {
        marginBottom: toRem(30),
        width: 'calc(25% - 15px)',
        marginRight: 20,

        '&:nth-of-type(3n), &:nth-of-type(2n)': {
            marginRight: 20
        },

        '&:nth-of-type(4n)': {
            marginRight: 0
        },

        '&:hover': {
            border: `1px solid ${theme.colors.green}`,

            '.controls .edit, .controls .delete': {
                opacity: 1
            }
        }
    },

    [theme.breakpoints.up('xl')]: {
        padding: toRems([20, 20, 30, 20])
    },

    '& .blockLink': {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        textIndent: -9999,
        zIndex: 1
    },

    '& .image-name': {
        display: 'flex',
        alignItems: 'center',

        [theme.breakpoints.up(580)]: {
            order: 2,
            flexDirection: 'column',
            marginTop: -10
        }
    },

    '& .image': {
        width: 43,
        height: 43,
        borderRadius: '50%',
        background: contactImage
            ? `url(${contactImage}) no-repeat center center`
            : `url(/images/contacts/default-profile-image.png) no-repeat center center`,
        backgroundSize: 'cover',
        border: `3px solid ${theme.colors.greyLight}`,
        marginRight: toRem(30),

        [theme.breakpoints.up(580)]: {
            marginRight: 0,
            width: 60,
            height: 60,
            marginBottom: toRem(15)
        },

        [theme.breakpoints.up('xl')]: {
            width: 90,
            height: 90
        }
    },

    '& .name': {
        fontSize: toRem(14),
        color: theme.colors.grey,
        fontWeight: 700,

        [theme.breakpoints.up(580)]: {
            fontSize: toRem(18)
        },

        [theme.breakpoints.up('xl')]: {
            fontSize: toRem(24)
        }
    },

    '& .controls': {
        display: 'flex',
        justifyContent: 'space-between',
        marginLeft: 'auto',
        width: 100,

        [theme.breakpoints.up(580)]: {
            order: 1,
            width: '100%'
        },

        '.edit': {
            width: 16,
            position: 'relative',

            [theme.breakpoints.up(580)]: {
                marginLeft: 'auto',
                marginRight: toRem(25)
            },

            [theme.breakpoints.up('xl')]: {
                width: 20
            }
        },

        '.edit, .delete': {
            zIndex: 2,
            [theme.breakpoints.up('lg')]: {
                opacity: 0,
                transition: 'opacity 0.5s ease'
            }
        }
    }
})

export default style
