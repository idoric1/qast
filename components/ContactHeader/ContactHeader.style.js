import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, variant, imageUrl }) => ({
    '& .mobile-controls': {
        padding: toRems([20, 30]),
        borderBottom: `1px solid ${theme.colors.greyLight}`,
        display: 'flex',
        alignItems: 'center',

        [theme.breakpoints.up('md')]: {
            display: 'none'
        },

        '.back, .favorite, .edit': {
            width: 16
        },

        '.delete': {
            width: 13
        },

        '.favorite, .delete': {
            marginLeft: 'auto'
        },

        '.favorite': {
            marginRight: toRem(40)
        }
    },

    '& .head': {
        padding: toRems([20, 0]),
        margin: toRems([0, 30]),
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottom: `1px solid ${theme.colors.green}`,
        maxWidth: 1130,

        [theme.breakpoints.up('md')]: {
            borderBottom: 'none',
            marginTop: toRem(40)
        },

        [theme.breakpoints.up('lg')]: {
            marginTop: toRem(80)
        },

        [theme.breakpoints.up(1190)]: {
            marginLeft: 'auto',
            marginRight: 'auto'
        }
    },

    '& .name-controls': {
        display: variant === 'add' || variant === 'edit' ? 'none' : 'flex',
        flexGrow: 1,
        alignItems: 'center',

        [theme.breakpoints.up('md')]: {
            borderBottom: `1px solid ${theme.colors.green}`,
            marginLeft: toRem(20),
            paddingBottom: toRem(20),
            display: variant === 'add' || variant === 'edit' ? 'flex' : null
        },

        [theme.breakpoints.up('lg')]: {
            paddingBottom: toRem(40),
            marginTop: toRem(-80)
        },

        '.back, .favorite, .edit': {
            display: 'none',
            width: 16,

            [theme.breakpoints.up('md')]: {
                display: 'block'
            }
        },

        '.back': {
            marginRight: toRem(20),

            [theme.breakpoints.up('lg')]: {
                width: 30,
                marginRight: toRem(30),
                marginLeft: toRem(15)
            }
        },

        '.favorite': {
            marginLeft: 'auto',
            marginRight: toRem(40),

            [theme.breakpoints.up('lg')]: {
                width: 20,
                marginRight: toRem(60)
            }
        },

        '.edit': {
            [theme.breakpoints.up('lg')]: {
                width: 20
            }
        },

        '.delete': {
            display: 'none',
            marginLeft: 'auto',

            [theme.breakpoints.up('md')]: {
                display: 'inline-flex',

                '.icon': {
                    width: 16,
                    marginLeft: toRem(10)
                }
            },

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(18),

                '.icon': {
                    width: 20
                }
            }
        }
    },

    '& .name': {
        fontSize: toRem(21),
        fontWeight: 700,
        color: theme.colors.grey,

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(36)
        }
    },

    '& .image': {
        width: 50,
        height: 50,
        borderRadius: '50%',
        background: imageUrl
            ? `url(${imageUrl}) no-repeat center center`
            : `url(/images/contacts/default-profile-image.png) no-repeat center center`,
        backgroundSize: 'cover',
        border: `3px solid ${theme.colors.greyLight}`,
        marginRight: toRem(30),

        [theme.breakpoints.up('md')]: {
            width: 150,
            height: 150
        },

        [theme.breakpoints.up('lg')]: {
            width: 240,
            height: 240
        }
    },

    '& .file-upload': {
        label: {
            width: 150,
            height: 150,
            borderRadius: '50%',
            background: imageUrl
                ? `url(${imageUrl}) no-repeat center center`
                : `${theme.colors.greenLight} url(/icons/upload.svg) no-repeat center center`,
            backgroundSize: imageUrl ? 'cover' : '20px auto',
            border: `3px solid ${theme.colors.greyLight}`,
            textIndent: -9999,
            display: 'block',
            position: 'relative',
            overflow: 'hidden',

            '&:before': {
                content: '""',
                width: '100%',
                height: '100%',
                position: 'absolute',
                top: 0,
                left: 0,
                background:
                    variant === 'edit' && imageUrl
                        ? 'rgba(0,0,0, 0.5) url(/icons/close.svg) no-repeat center center'
                        : null
            },

            [theme.breakpoints.up('lg')]: {
                width: 240,
                height: 240,
                backgroundSize: imageUrl ? 'cover' : '30px auto',
                marginTop: -20
            }
        },

        input: {
            width: '0.1px',
            height: '0.1px',
            opacity: 0,
            overflow: 'hidden',
            position: 'absolute',
            zIndex: -1
        }
    }
})

export default style
