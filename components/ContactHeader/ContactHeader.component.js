import { withTheme } from 'emotion-theming'
import { useState, useEffect } from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import Link from 'next/link'
import ContactContextWrapper from 'components/ContactContextWrapper'

import EditIcon from 'public/icons/edit.svg'
import BackArrowIcon from 'public/icons/back-arrow.svg'

import FavoriteButton from 'components/FavoriteButton'
import DeleteButton from 'components/DeleteButton'
import ConfirmationDialog from 'components/ConfirmationDialog'

import style from './ContactHeader.style'

const ContactHeaderStyled = styled.div(props => ({ ...style(props) }))

const ContactHeader = ({ variant, fullName, imageUrl, contactId, imageCallback, isFavorite }) => {
    const [image, setImage] = useState('')
    const isAddOrEdit = variant === 'edit' || variant === 'add'

    const handleImage = e => {
        setImage(e.target.files[0])
    }

    useEffect(() => {
        imageCallback(image)
    }, [image])

    return (
        <ContactHeaderStyled variant={variant} imageUrl={imageUrl}>
            <ContactContextWrapper>
                <div className="mobile-controls">
                    <Link href="/">
                        <a className="back">
                            <BackArrowIcon />
                        </a>
                    </Link>
                    {variant === 'view' && (
                        <div className="favorite">
                            <FavoriteButton id={contactId} isFavorite={isFavorite} />
                        </div>
                    )}

                    {variant === 'view' && (
                        <Link href="/contact/[id]/edit" as={`/contact/${contactId}/edit`}>
                            <a className="edit">
                                <EditIcon />
                            </a>
                        </Link>
                    )}

                    {variant === 'edit' && (
                        <div className="delete">
                            <DeleteButton id={contactId} onPage />
                        </div>
                    )}
                </div>

                <div className="head">
                    {variant === 'view' && <div className="image" />}
                    {isAddOrEdit && (
                        <div className="file-upload">
                            <label htmlFor="file">Choose a file</label>
                            <input type="file" onChange={e => handleImage(e)} name="file" id="file" />
                        </div>
                    )}
                    <div className="name-controls">
                        <Link href="/">
                            <a className="back">
                                <BackArrowIcon />
                            </a>
                        </Link>

                        {variant === 'view' && <div className="name">{fullName}</div>}

                        {variant === 'view' && (
                            <div className="favorite">
                                <FavoriteButton id={contactId} isFavorite={isFavorite} />
                            </div>
                        )}

                        {variant === 'view' && (
                            <Link href="/contact/[id]/edit" as={`/contact/${contactId}/edit`}>
                                <a className="edit">
                                    <EditIcon />
                                </a>
                            </Link>
                        )}

                        {variant === 'edit' && (
                            <div className="delete">
                                <DeleteButton id={contactId} onPage hasText />
                            </div>
                        )}
                    </div>
                </div>

                <ConfirmationDialog
                    title="Delete"
                    text="Are you sure you want to delete this contact?"
                    confirmButtonText="Delete"
                    onPage
                />
            </ContactContextWrapper>
        </ContactHeaderStyled>
    )
}

ContactHeader.propTypes = {
    variant: PropTypes.string.isRequired,
    fullName: PropTypes.string,
    imageUrl: PropTypes.string,
    contactId: PropTypes.number,
    imageCallback: PropTypes.func,
    isFavorite: PropTypes.bool
}

ContactHeader.defaultProps = {
    fullName: null,
    imageUrl: null,
    contactId: null,
    imageCallback: () => {},
    isFavorite: false
}

export default withTheme(ContactHeader)
