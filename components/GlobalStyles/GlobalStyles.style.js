import { createFontVariations, toRem, toRems } from 'helpers/theme'
import merge from 'deepmerge'

/* eslint-disable import/no-absolute-path, import/extensions, quote-props, max-len */
const style = theme => {
    const { fontSizes, rootFontSize } = theme.typography
    const globals = {
        '.isHidden': {
            display: 'none'
        },
        '.no-bullet': {
            padding: 0,
            margin: 0,
            listStyle: 'none'
        }
    }

    return merge.all([
        theme.normalize,
        theme.typography,
        createFontVariations({ fontSizes, rootFontSize }),
        theme.global,
        globals
    ])
}

export default style
