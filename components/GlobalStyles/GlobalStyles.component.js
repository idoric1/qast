/** @jsx jsx */
import { Global, jsx, css } from '@emotion/core'
import { withTheme } from 'emotion-theming'
import PropTypes from 'prop-types'

import style from './GlobalStyles.style'

const GlobalStyles = ({ theme }) => (
    <>
        {/* eslint-disable react/prop-types */}
        <Global
            styles={css`
                html,
                body {
                    background: #ffffff;
                }

                .block-link {
                    position: absolute;
                    top: 0;
                    left: 0;
                    width: 100%;
                    height: 100%;
                    z-index: 1;
                    outline: none;
                    cursor: pointer;
                    text-indent: -9999px;
                }
            `}
        />
        <Global styles={{ ...style(theme) }} />
    </>
)

/* eslint-disable react/require-default-props */
GlobalStyles.propTypes = {
    theme: PropTypes.shape({}).isRequired
}

export default withTheme(GlobalStyles)
