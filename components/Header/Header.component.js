import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'

import Brand from 'public/images/brand.svg'

import style from './Header.style'

const HeaderStyled = styled.header(props => ({ ...style(props) }))

const Header = () => {
    return (
        <HeaderStyled>
            <div className="brand-wrap"><Brand /></div>
        </HeaderStyled>
    )
}

export default withTheme(Header)
