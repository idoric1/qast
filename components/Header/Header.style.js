import { toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    background: 'linear-gradient(to right, #37a1ab 0%, #79c9ce 100%)',
    position: 'relative',
    textAlign: 'center',
    padding: toRems([15, 0, 17, 0]),

    [theme.breakpoints.up('md')]: {
        padding: toRems([18, 0, 20, 0])
    },

    '&:after': {
        content: '""',
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        height: 7,
        background: 'linear-gradient(to right, #70bbc4 0%, #a1d8dc 100%)'
    },

    '.brand-wrap': {
        display: 'inline-block',
        width: 150,

        [theme.breakpoints.up('md')]: {
            width: 250
        }
    }
})

export default style
