import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import { useState, useContext, useEffect } from 'react'
import { useUIDSeed } from 'react-uid'
import { Box, Flex } from 'reflexbox'
import PropTypes from 'prop-types'

import AppContext from 'contexts/AppContext'

import ContactHeader from 'components/ContactHeader'
import Input from 'components/Input'
import Button from 'components/Button'

import PlusIcon from 'public/icons/plus.svg'
import PhoneIcon from 'public/icons/phone.svg'

import layoutTheme from 'theme/layout-theme'

import style from './AddEditForm.style'

const AddEditFormStyled = styled.div(props => ({ ...style(props) }))

const AddEditForm = ({ contact }) => {
    const uid = useUIDSeed()
    const { contacts, setContacts } = useContext(AppContext)
    const [numberFields, setNumberFields] = useState(
        contact ? contact.numbers : [{ id: Date.now(), label: '', number: '' }]
    )
    const [fullName, setFullName] = useState(contact ? contact.fullName : '')
    const [email, setEmail] = useState(contact ? contact.email : '')
    const [imageUrl, setImageUrl] = useState(contact ? contact.imageUrl : '')
    const [formErrors, setFormErrors] = useState({})

    const imageCallback = file => {
        if (file) {
            const imgReader = new FileReader()
            imgReader.onloadend = () => {
                setImageUrl(imgReader.result)
            }
            imgReader.readAsDataURL(file)
        }
    }

    const validateForm = values => {
        const errors = {}

        const validateNumber = number => {
            if (!number) {
                return 'Number is required'
            }
            // Super basic phone validation. Nothing to see here
            if (!/^[0-9 ()+]{6,20}$/i.test(number)) {
                return 'Number is invalid'
            }

            return null
        }

        if (!values.fullName) {
            errors.fullName = 'Full name is required!'
        }

        if (!values.email) {
            errors.email = 'Email is required!'
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
            errors.email = 'Email is invalid'
        }

        if (values.numbers) {
            values.numbers.forEach(item => {
                if (!item.label || validateNumber(item.number)) {
                    if (!errors.numbers) errors.numbers = []
                    errors.numbers.push({
                        id: item.id,
                        label: !item.label ? 'Label is required' : null,
                        number: validateNumber(item.number)
                    })
                }
            })
        }

        setFormErrors(errors)
        return Object.keys(errors).length === 0 && errors.constructor === Object
    }

    const handleNumbersErrors = (id, type) => {
        return formErrors.numbers && formErrors.numbers.find(i => i.id === id)
            ? formErrors.numbers.find(i => i.id === id)[type]
            : null
    }

    const handleSubmit = e => {
        e.preventDefault()

        const result = {
            id: contact ? contact.id : Date.now(),
            imageUrl,
            fullName,
            email,
            numbers: numberFields,
            visible: true,
            favorite: false
        }

        if (validateForm(result)) {
            if (contact) {
                setContacts([...contacts].map(obj => (obj.id === result.id ? result : obj)))
            } else {
                setContacts([...contacts, result])
            }

            window.history.back()
        }
    }

    useEffect(() => {
        localStorage.setItem('contacts', JSON.stringify(contacts))
    }, [contacts])

    const handleAddField = () => {
        setNumberFields([...numberFields, { id: Date.now(), label: '', number: '' }])
    }

    const handleRemoveField = id => {
        const filtered = numberFields.filter(item => item.id !== id)
        setNumberFields(filtered)
    }

    const handleNumbers = (value, id, type) => {
        const temp = [...numberFields]

        if (type === 'number') {
            temp.find(item => item.id === id).number = value
            return
        }

        temp.find(item => item.id === id).label = value

        setNumberFields(temp)
    }

    return (
        <AddEditFormStyled>
            <form onSubmit={handleSubmit} noValidate>
                <ContactHeader
                    variant={contact ? 'edit' : 'add'}
                    imageCallback={imageCallback}
                    imageUrl={imageUrl}
                    contactId={contact ? contact.id : null}
                />

                <Box
                    theme={layoutTheme}
                    variant="crudContainer"
                    mt={[null, null, null, -80, -140]}
                    mb={[70, null, null, 140]}>
                    <Input
                        id="full-name"
                        name="full-name"
                        label="full name"
                        type="text"
                        placeholder="Full name"
                        labelIcon="person"
                        onChange={e => setFullName(e.target.value)}
                        value={fullName}
                        error={formErrors.fullName}
                        bottomBorder
                    />
                    <Input
                        id="email"
                        name="email"
                        label="email"
                        type="email"
                        placeholder="Email"
                        labelIcon="email"
                        onChange={e => setEmail(e.target.value)}
                        value={email}
                        error={formErrors.email}
                        bottomBorder
                    />

                    <label className="label">
                        <span className="icon">
                            <PhoneIcon />
                        </span>{' '}
                        numbers
                    </label>

                    {numberFields.map((item, index) => (
                        <div key={uid(index)} className="add-number-item">
                            <Input
                                id={`numberLabel-${index}`}
                                name={`numberLabel-${index}`}
                                type="text"
                                onBlur={e => handleNumbers(e.target.value, item.id, 'label')}
                                placeholder="Label"
                                value={item.label}
                                error={handleNumbersErrors(item.id, 'label')}
                                noPadding
                            />
                            <div className="input-remove">
                                <Input
                                    id={`number-${index}`}
                                    name={`number-${index}`}
                                    type="text"
                                    onBlur={e => handleNumbers(e.target.value, item.id, 'number')}
                                    placeholder="Number"
                                    value={item.number}
                                    error={handleNumbersErrors(item.id, 'number')}
                                    noPadding
                                />
                                <button className="remove" type="button" onClick={() => handleRemoveField(item.id)}>
                                    <span>&#65794;</span>
                                </button>
                            </div>
                        </div>
                    ))}

                    <div className="add-wrap">
                        <button className="add-number-button" onClick={() => handleAddField()} type="button">
                            <span className="icon-wrap">
                                <span className="icon">
                                    <PlusIcon />
                                </span>
                            </span>
                            <span className="label">Add Number</span>
                        </button>
                    </div>
                </Box>

                <Flex
                    theme={layoutTheme}
                    variant="crudContainer"
                    mb={[70, null, null, 140]}
                    justifyContent="space-between">
                    <Button variant="secondary" onClick={() => window.history.back()}>
                        Cancel
                    </Button>
                    <Button type="button">Save</Button>
                </Flex>
            </form>
        </AddEditFormStyled>
    )
}

AddEditForm.propTypes = {
    contact: PropTypes.shape({
        id: PropTypes.number,
        fullName: PropTypes.string,
        email: PropTypes.string,
        imageUrl: PropTypes.string,
        favorite: PropTypes.bool,
        numbers: PropTypes.arrayOf(
            PropTypes.shape({
                label: PropTypes.string.isRequired,
                number: PropTypes.string.isRequired
            })
        )
    })
}

AddEditForm.defaultProps = {
    contact: null
}

export default withTheme(AddEditForm)
