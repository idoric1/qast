import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    '.label': {
        marginTop: toRem(20),
        display: 'flex',
        color: theme.colors.green,
        fontWeight: 700,
        fontSize: toRem(14),
        marginBottom: toRem(15),

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(21),
            marginBottom: toRem(20)
        },

        '.icon': {
            width: 15,
            marginRight: toRem(15),

            [theme.breakpoints.up('lg')]: {
                width: 20,
                marginRight: toRem(20)
            }
        }
    },

    '& .add-number-item': {
        [theme.breakpoints.up('lg')]: {
            display: 'flex',

            '& > div': {
                flexGrow: 1
            }
        },

        input: {
            marginBottom: toRem(10),

            [theme.breakpoints.up('lg')]: {
                marginBottom: 0
            }
        },

        '.input-remove': {
            display: 'flex',
            alignItems: 'center',
            maxWidth: 400,
            marginBottom: toRem(30),

            [theme.breakpoints.up('lg')]: {
                marginLeft: 40,
                flexGrow: 1,
                maxWidth: 430,

                '& > div': {
                    flexGrow: 1
                }
            }
        },

        '.remove': {
            width: 31,
            height: 31,
            border: `1px solid ${theme.colors.greyLight}`,
            borderRadius: '50%',
            color: theme.colors.greyLight,
            marginLeft: toRem(20),
            position: 'relative',

            [theme.breakpoints.up('lg')]: {
                width: 40,
                height: 40
            },

            span: {
                position: 'absolute',
                top: 11,
                left: '50%',
                transform: 'translateX(-50%)',
                fontSize: toRem(21),
                lineHeight: 0,

                [theme.breakpoints.up('lg')]: {
                    top: 13,
                    fontSize: toRem(28)
                }
            }
        }
    },

    '& .add-wrap': {
        marginTop: toRem(40),

        [theme.breakpoints.up('lg')]: {
            marginTop: toRem(60)
        }
    },

    '& .add-number-button': {
        display: 'inline-flex',
        alignItems: 'center',

        '.icon-wrap': {
            width: 31,
            height: 31,
            border: `1px solid ${theme.colors.green}`,
            borderRadius: '50%',
            position: 'relative',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',

            [theme.breakpoints.up('lg')]: {
                width: 40,
                height: 40
            },

            '.icon': {
                display: 'inline-block',
                width: 8,

                [theme.breakpoints.up('lg')]: {
                    width: 12
                }
            }
        },

        '.label': {
            fontSize: toRem(14),
            color: theme.colors.green,
            marginLeft: toRem(15),

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(18)
            }
        }
    }
})

export default style
