import { useState } from 'react'
import PropTypes from 'prop-types'
import ContactContext from 'contexts/ContactContext'

const ContactContextWrapper = ({ children }) => {
    const [confirmationDialog, setConfirmationDialog] = useState(false)
    const [confirm, setConfirm] = useState(false)

    return (
        <ContactContext.Provider value={{ confirmationDialog, setConfirmationDialog, confirm, setConfirm }}>
            {children}
        </ContactContext.Provider>
    )
}

ContactContextWrapper.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
}

ContactContextWrapper.defaultProps = {
    children: undefined
}

export default ContactContextWrapper
