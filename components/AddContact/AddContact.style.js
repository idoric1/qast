import { toRem } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    height: 65,
    position: 'relative',
    marginBottom: toRem(10),
    opacity: 0.5,

    [theme.breakpoints.up(580)]: {
        width: 'calc(50% - 10px)',
        height: 'auto',
        minHeight: 145,
        marginBottom: toRem(20),
        marginRight: 20
    },

    [theme.breakpoints.up('md')]: {
        width: 'calc(33.3333% - 13px)',
        marginRight: 19.8
    },

    [theme.breakpoints.up('lg')]: {
        width: 'calc(25% - 15px)',
        marginBottom: toRem(30),
        marginRight: 20,
        minHeight: 147
    },

    [theme.breakpoints.up('xl')]: {
        minHeight: 202
    },

    '& a': {
        display: 'flex',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        border: `1px dashed ${theme.colors.green}`,
        borderRadius: 4,
        padding: toRem(20),

        [theme.breakpoints.up(580)]: {
            flexDirection: 'column',
            justifyContent: 'center'
        },

        '.icon': {
            width: 18,

            [theme.breakpoints.up('xl')]: {
                width: 24
            }
        },

        '.label': {
            marginLeft: toRem(20),
            fontSize: toRem(14),
            color: theme.colors.green,

            [theme.breakpoints.up(580)]: {
                marginLeft: 0,
                marginTop: toRem(20),
                fontSize: toRem(16)
            }
        }
    }
})

export default style
