import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import Link from 'next/link'

import PlusIcon from 'public/icons/plus.svg'

import style from './AddContact.style'

const AddContactStyled = styled.div(props => ({ ...style(props) }))

const AddContact = () => {
    return (
        <AddContactStyled>
            <Link href="/contact/add">
                <a>
                    <span className="icon"><PlusIcon /></span> <span className="label">Add New</span>
                </a>
            </Link>
        </AddContactStyled>
    )
}

export default withTheme(AddContact)
