import { withTheme } from 'emotion-theming'
import { useState } from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'

import PersonIcon from 'public/icons/person.svg'
import PhoneIcon from 'public/icons/phone.svg'
import EmailIcon from 'public/icons/email.svg'
import style from './Input.style'

const InputStyled = styled.div(props => ({ ...style(props) }))

const Input = ({
    type,
    label,
    name,
    id,
    value,
    error,
    required,
    onChange,
    onBlur,
    disabled,
    placeholder,
    bottomBorder,
    labelIcon,
    noPadding
}) => {
    const [active, setActive] = useState(!!error)
    const [inputValue, setInputValue] = useState(value)

    const handleOnChange = event => {
        setInputValue(event.target.value)
        onChange(event)
    }

    return (
        <InputStyled error={error} bottomBorder={bottomBorder} noPadding={noPadding}>
            <div className="wrap">
                {label && (
                    <label htmlFor={id} className={active ? 'isActive' : ''}>
                        {labelIcon && labelIcon === 'person' && (
                            <div className="icon">
                                <PersonIcon />
                            </div>
                        )}
                        {labelIcon && labelIcon === 'phone' && (
                            <div className="icon">
                                <PhoneIcon />
                            </div>
                        )}
                        {labelIcon && labelIcon === 'email' && (
                            <div className="icon">
                                <EmailIcon />
                            </div>
                        )}
                        {label}
                    </label>
                )}

                <div className="inner">
                    <input
                        id={id}
                        type={type}
                        name={name}
                        onChange={event => handleOnChange(event)}
                        onBlur={onBlur}
                        onFocus={() => setActive(true)}
                        value={inputValue}
                        disabled={disabled}
                        required={required}
                        placeholder={placeholder}
                    />

                    {error && (
                        <div className="input-error" role="alert">
                            {error}
                        </div>
                    )}
                </div>
            </div>
        </InputStyled>
    )
}

Input.propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    name: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string,
    error: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
    onBlur: PropTypes.func,
    placeholder: PropTypes.string,
    bottomBorder: PropTypes.bool,
    labelIcon: PropTypes.string,
    noPadding: PropTypes.bool
}

Input.defaultProps = {
    type: 'text',
    value: undefined,
    error: null,
    required: false,
    disabled: false,
    placeholder: null,
    onChange: () => {},
    onBlur: () => {},
    bottomBorder: false,
    labelIcon: null,
    label: null,
    noPadding: false
}

export default withTheme(Input)
