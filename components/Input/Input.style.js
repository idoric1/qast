import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, bottomBorder, noPadding, error }) => ({
    padding: noPadding ? null : toRems([20, 0]),
    borderBottom: bottomBorder ? `1px solid ${theme.colors.green}` : null,

    [theme.breakpoints.up('lg')]: {
        padding: noPadding ? null : toRems([30, 0, 40, 0])
    },

    '& label': {
        display: 'flex',
        color: theme.colors.green,
        fontWeight: 700,
        fontSize: toRem(14),
        marginBottom: toRem(15),

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(21)
        },

        '.icon': {
            width: 15,
            marginRight: toRem(15),

            [theme.breakpoints.up('lg')]: {
                width: 20,
                marginRight: toRem(20)
            }
        }
    },

    '& .inner': {
        position: 'relative'
    },

    input: {
        width: '100%',
        maxWidth: 400,
        height: 50,
        border: error ? `1px solid ${theme.colors.red}` : `1px solid ${theme.colors.greyLight}`,
        borderRadius: 4,
        padding: toRems([0, 20]),
        fontSize: toRem(14),
        color: theme.colors.grey,

        [theme.breakpoints.up('lg')]: {
            height: 80,
            fontSize: toRem(21)
        },

        '&::placeholder': {
            fontSize: toRem(14),
            color: theme.colors.grey,

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(21)
            }
        }
    },

    '& .input-error': {
        position: 'absolute',
        top: 0,
        left: 0,
        background: theme.colors.red,
        color: theme.colors.white,
        padding: '1px 5px 2px 5px',
        fontSize: toRem(11),
        fontWeight: 700,
        borderRadius: '4px 0 4px 0',

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(14),
            padding: '3px 10px 3px 10px'
        }
    }
})

export default style
