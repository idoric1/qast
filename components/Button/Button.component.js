import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'

import style from './Button.style'

const ButtonStyled = styled.button(props => ({ ...style(props) }))

const Button = ({ children, variant, small, onClick }) => {
    return <ButtonStyled variant={variant} small={small} onClick={onClick}>{children}</ButtonStyled>
}

Button.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
    variant: PropTypes.string,
    small: PropTypes.bool,
    onClick: PropTypes.func
}

Button.defaultProps = {
    variant: 'primary',
    small: false,
    onClick: () => {}
}

export default withTheme(Button)
