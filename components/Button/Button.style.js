import { toRem } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, variant, small }) => ({
    minWidth: 145,
    height: 42,
    background: variant === 'secondary' ? theme.colors.grey : theme.colors.green,
    color: theme.colors.white,
    fontSize: toRem(14),
    fontWeight: 700,
    borderRadius: 100,

    [theme.breakpoints.up('lg')]: {
        minWidth: small ? null : 210,
        height: small ? null : 60,
        fontSize: small ? null : toRem(18)
    }
})

export default style
