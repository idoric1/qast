import { withTheme } from 'emotion-theming'
import { useContext, useEffect } from 'react'
import { useRouter } from 'next/router'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import AppContext from 'contexts/AppContext'
import ContactContext from 'contexts/ContactContext'

import DeleteIcon from 'public/icons/trash.svg'

import style from './DeleteButton.style'

const DeleteButtonStyled = styled.div(props => ({ ...style(props) }))

const DeleteButton = ({ id, onPage, hasText }) => {
    const router = useRouter()
    const { contacts, setContacts } = useContext(AppContext)
    const { setContactToBeDeleted } = useContext(AppContext)
    const { setConfirmationDialog } = useContext(ContactContext)
    const { confirm } = useContext(ContactContext)

    // Delete if user confirms
    useEffect(() => {
        if (confirm) {
            if (onPage) {
                setContactToBeDeleted(id)
                router.push('/')
                return
            }

            const filtered = [...contacts].filter(item => {
                return item.id !== id
            })

            localStorage.setItem('contacts', JSON.stringify(filtered))
            setContacts(filtered)
        }
    }, [confirm])

    return (
        <DeleteButtonStyled>
            <button type="button" onClick={() => setConfirmationDialog(true)}>
                {hasText && <span className="text">Delete</span>}{' '}
                <span className="icon">
                    <DeleteIcon />
                </span>
            </button>
        </DeleteButtonStyled>
    )
}

DeleteButton.propTypes = {
    id: PropTypes.number.isRequired,
    onPage: PropTypes.bool,
    hasText: PropTypes.bool
}

DeleteButton.defaultProps = {
    onPage: false,
    hasText: false
}

export default withTheme(DeleteButton)
