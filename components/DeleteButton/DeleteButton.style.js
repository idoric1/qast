import { toRem } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    '& button': {
        alignItems: 'center',
        display: 'inline-flex',
        color: theme.colors.grey,
        fontSize: toRem(14),

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(18)
        },

        '.icon': {
            width: 13,
            display: 'inline-block',
            [theme.breakpoints.up('xl')]: {
                width: 16
            }
        }
    }
})

export default style
