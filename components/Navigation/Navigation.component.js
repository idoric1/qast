import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import Link from 'next/link'
import { useUIDSeed } from 'react-uid'
import { useRouter } from 'next/router'

import style from './Navigation.style'

const NavigationStyled = styled.nav(props => ({ ...style(props) }))

const Navigation = ({ items }) => {
    const uid = useUIDSeed()
    const router = useRouter()

    return (
        <NavigationStyled>
            <ul className="navigation">
                {items.map(item => (
                    <li key={uid(item)}>
                        <Link href={item.url}>
                            <a className={item.url === router.route ? 'active' : ''}>{item.label}</a>
                        </Link>
                    </li>
                ))}
            </ul>
        </NavigationStyled>
    )
}

Navigation.propTypes = {
    items: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired
        })
    ).isRequired
}

export default withTheme(Navigation)
