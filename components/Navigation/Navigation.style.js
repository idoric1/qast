import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    '& .navigation': {
        listStyle: 'none',
        margin: 0,
        display: 'flex',
        justifyContent: 'center',
        padding: toRems([15, 0]),
        borderBottom: `1px solid ${theme.colors.green}`,

        [theme.breakpoints.up('lg')]: {
            padding: toRems([50, 0])
        },

        [theme.breakpoints.up('xl')]: {
            padding: toRems([70, 0])
        },

        li: {
            flexBasis: '50%',
            textAlign: 'center',
            position: 'relative',

            [theme.breakpoints.up('md')]: {
                maxWidth: 200
            },

            '&:after': {
                content: '""',
                position: 'absolute',
                top: '50%',
                left: 0,
                width: 1,
                height: 14,
                transform: 'translateY(-50%)',
                background: theme.colors.grey,

                [theme.breakpoints.up('xl')]: {
                    height: 20
                }
            },

            '&:first-of-type:after': {
                display: 'none'
            }
        },

        a: {
            color: theme.colors.grey,
            fontWeight: 700,
            fontSize: toRem(14),

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(18)
            },

            [theme.breakpoints.up('xl')]: {
                fontSize: toRem(24)
            },

            '&.active': {
                color: theme.colors.green
            }
        }
    }
})

export default style
