export default () => [
    <link key="0" rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png" />,
    <link key="1" rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png" />,
    <link key="2" rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png" />,
    <link key="5" rel="shortcut icon" href="/favicon/favicon.ico" />,
    <meta key="6" name="msapplication-TileColor" content="#da532c" />,
    <meta key="7" name="msapplication-config" content="/favicon/browserconfig.xml" />,
    <meta key="8" name="theme-color" content="#ffffff" />
]
