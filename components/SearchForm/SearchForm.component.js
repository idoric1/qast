import { withTheme } from 'emotion-theming'
import { useContext, useEffect } from 'react'
import styled from '@emotion/styled'
import AppContext from 'contexts/AppContext'
import PropTypes from 'prop-types'

import style from './SearchForm.style'

const SearchFormStyled = styled.div(props => ({ ...style(props) }))

const SearchForm = ({ onFavorites }) => {
    const { setContacts } = useContext(AppContext)

    const handleSearch = value => {
        let contacts = ''

        if (onFavorites) {
            contacts = JSON.parse(localStorage.getItem('contacts')).filter(item => item.favorite === true)
        } else {
            contacts = JSON.parse(localStorage.getItem('contacts'))
        }

        const filtered = contacts.filter(item => item.fullName.toLowerCase().includes(value.toLowerCase()))
        setContacts(filtered)
    }

    useEffect(() => {
        return function cleanup() {
            setContacts(JSON.parse(localStorage.getItem('contacts')))
        }
    }, [])

    return (
        <SearchFormStyled>
            <input type="text" onChange={e => handleSearch(e.target.value)} />
        </SearchFormStyled>
    )
}

SearchForm.propTypes = {
    onFavorites: PropTypes.bool
}

SearchForm.defaultProps = {
    onFavorites: false
}

export default withTheme(SearchForm)
