import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    textAlign: 'center',
    input: {
        width: '100%',
        maxWidth: 540,
        height: 50,
        border: `1px solid ${theme.colors.greyLight}`,
        borderRadius: 4,
        boxShadow: '0 2px 34px 0 #E3E3E3',
        background: 'url(icons/search.svg) no-repeat 20px center',
        paddingLeft: 50,
        fontSize: toRem(16),
        color: theme.colors.grey,

        [theme.breakpoints.up('md')]: {
            height: 60
        },

        [theme.breakpoints.up('lg')]: {
            maxWidth: 720
        },

        [theme.breakpoints.up('xl')]: {
            height: 80,
            backgroundSize: '23px auto'
        }
    }
})

export default style
