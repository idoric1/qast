import { withTheme } from 'emotion-theming'
import { useContext } from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import ContactContext from 'contexts/ContactContext'

import Button from 'components/Button'

import style from './ConfirmationDialog.style'

const ConfirmationDialogStyled = styled.div(props => ({ ...style(props) }))

const ConfirmationDialog = ({ title, text, confirmButtonText, onPage }) => {
    const { confirmationDialog, setConfirmationDialog } = useContext(ContactContext)
    const { setConfirm } = useContext(ContactContext)

    const handleConfirm = () => {
        setConfirm(true)
        setConfirmationDialog(false)
    }

    return (
        <ConfirmationDialogStyled>
            {confirmationDialog && (
                <div className="backdrop">
                    <div className="modal" role="dialog">
                        <header className="title">{title}</header>
                        <div className="body">
                            <p>{text}</p>
                        </div>

                        <div className="buttons">
                            <Button variant="secondary" onClick={() => setConfirmationDialog(false)} small>
                                Cancel
                            </Button>
                            <Button small onClick={() => handleConfirm()}>
                                {confirmButtonText}
                            </Button>
                        </div>
                    </div>
                </div>
            )}
        </ConfirmationDialogStyled>
    )
}

ConfirmationDialog.propTypes = {
    confirmButtonText: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onPage: PropTypes.bool
}

ConfirmationDialog.defaultProps = {
    onPage: false
}

export default withTheme(ConfirmationDialog)
