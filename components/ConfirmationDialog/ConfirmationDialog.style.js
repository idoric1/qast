import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme }) => ({
    '& .backdrop': {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0,0,0,0.5)',
        zIndex: 99,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },

    '& .modal': {
        width: 315,
        background: theme.colors.white,
        borderRadius: 4,

        [theme.breakpoints.up('md')]: {
            width: 480
        },

        header: {
            padding: toRem(20),
            fontWeight: 700,
            color: theme.colors.grey,
            borderBottom: `1px solid ${theme.colors.greyLight}`,

            [theme.breakpoints.up('md')]: {
                padding: toRem(30)
            }
        },

        '.body': {
            padding: toRems([40, 20]),
            color: theme.colors.grey,
            textAlign: 'center',

            [theme.breakpoints.up('md')]: {
                padding: toRems([30, 30])
            }
        },

        '.buttons': {
            padding: toRem(20),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',

            [theme.breakpoints.up('md')]: {
                padding: toRem(30),
                flexDirection: 'row',
                justifyContent: 'space-between'
            },

            'button:first-of-type': {
                order: 2,
                marginTop: toRem(20),

                [theme.breakpoints.up('md')]: {
                    order: 0,
                    marginTop: 0
                }
            }
        }
    }
})

export default style
