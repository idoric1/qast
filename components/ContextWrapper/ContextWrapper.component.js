import { useState } from 'react'
import PropTypes from 'prop-types'
import AppContext from 'contexts/AppContext'

import initialContacts from 'public/data/contacts'

const ContextWrapper = ({ children }) => {
    const initialData = () => {
        if (localStorage.getItem('contacts') === null) {
            localStorage.setItem('contacts', JSON.stringify(initialContacts))
        }

        return JSON.parse(localStorage.getItem('contacts'))
    }

    const [contacts, setContacts] = useState(initialData)
    const [filteredContacts, setFilteredContacts] = useState(initialData)
    const [contactToBeDeleted, setContactToBeDeleted] = useState(null)

    return (
        <AppContext.Provider
            value={{
                contacts,
                setContacts,
                contactToBeDeleted,
                setContactToBeDeleted,
                filteredContacts,
                setFilteredContacts
            }}>
            {children}
        </AppContext.Provider>
    )
}

ContextWrapper.propTypes = {
    children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node])
}

ContextWrapper.defaultProps = {
    children: undefined
}

export default ContextWrapper
