import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, variant}) => ({
    padding: toRems([25, 50, 0, 50]),
    marginBottom: toRem(50),

    [theme.breakpoints.up('md')]: {
        marginLeft: toRem(200),
        marginTop: variant === 'email' ? toRem(-50) : null
    },

    [theme.breakpoints.up('lg')]: {
        display: 'flex',
        alignItems: 'flex-start',
        maxWidth: 1130,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: variant === 'email' ? toRem(-80) : null,
        padding: toRems([25, 0, 0, 400])
    },

    '& .label': {
        display: 'flex',
        alignItems: 'center',
        marginBottom: toRem(20),

        [theme.breakpoints.up('lg')]: {
            minWidth: 150,
            fontSize: toRem(21)
        },

        '.icon': {
            width: 15,
            marginRight: toRem(10),

            [theme.breakpoints.up('lg')]: {
                width: 20
            }
        },

        span: {
            color: theme.colors.green,
            fontWeight: 700,
            marginTop: variant === 'numbers' ? -3 : null,

            [theme.breakpoints.up('lg')]: {
                marginTop: variant === 'numbers' ? -6 : -2,
            }
        }
    },

    '& .data': {
        marginLeft: toRem(25)
    },

    '& .email': {
        color: theme.colors.grey,

        [theme.breakpoints.up('lg')]: {
            fontSize: toRem(21)
        }
    },

    '& .number-item': {
        color: theme.colors.grey,
        display: 'flex',
        alignItems: 'center',
        marginBottom: toRem(30),

        [theme.breakpoints.up('lg')]: {
            marginBottom: toRem(70)
        },

        '.number-label': {
            textTransform: 'uppercase',
            fontSize: toRem(14),
            minWidth: 80,
            marginRight: toRem(20),
            fontWeight: 700,

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(18),
                minWidth: 100
            }
        },

        '.number': {
            marginTop: -5,

            [theme.breakpoints.up('lg')]: {
                fontSize: toRem(21)
            }
        },

        a: {
            color: theme.colors.grey,
            textDecoration: 'underline'
        }
    }
})

export default style
