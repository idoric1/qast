import { withTheme } from 'emotion-theming'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import { useUIDSeed } from 'react-uid'

import EmailIcon from 'public/icons/email.svg'
import PhoneIcon from 'public/icons/phone.svg'

import style from './DataDisplay.style'

const DataDisplayStyled = styled.div(props => ({ ...style(props) }))

const DataDisplay = ({ variant, email, numbers }) => {
    const uid = useUIDSeed()

    return (
        <DataDisplayStyled variant={variant}>
            <div className="label">
                {email && (
                    <>
                        <div className="icon">
                            <EmailIcon />
                        </div>{' '}
                        <span>email</span>
                    </>
                )}

                {numbers && numbers.length > 0 && (
                    <>
                        <div className="icon">
                            <PhoneIcon />
                        </div>{' '}
                        <span>numbers</span>
                    </>
                )}
            </div>

            <div className="data">
                {email && <span className="email">{email}</span>}

                {numbers &&
                    numbers.map(item => (
                        <div key={uid(item)} className="number-item">
                            <div className="number-label">{item.label}</div>
                            <div className="number">
                                <a href={`tel: ${item.number}`}>{item.number}</a>
                            </div>
                        </div>
                    ))}
            </div>
        </DataDisplayStyled>
    )
}

DataDisplay.propTypes = {
    variant: PropTypes.string.isRequired,
    email: PropTypes.string,
    numbers: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            number: PropTypes.string.isRequired
        })
    )
}

DataDisplay.defaultProps = {
    email: null,
    numbers: null
}

export default withTheme(DataDisplay)
