import { toRem, toRems } from 'helpers/theme'

/* eslint-disable quote-props */
const style = ({ theme, isFavorite }) => ({
    '& button svg path': {
        fill: isFavorite ? theme.colors.grey : theme.colors.white
    },

    '& button': {
        width: 16,
        position: 'relative',
        zIndex: 2,

        [theme.breakpoints.up('xl')]: {
            width: 20
        }
    }
})

export default style
