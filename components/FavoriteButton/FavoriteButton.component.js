import { withTheme } from 'emotion-theming'
import { useContext, useEffect, useState } from 'react'
import styled from '@emotion/styled'
import PropTypes from 'prop-types'
import AppContext from 'contexts/AppContext'

import FavoriteIcon from 'public/icons/heart.svg'

import style from './FavoriteButton.style'

const FavoriteButtonStyled = styled.div(props => ({ ...style(props) }))

const FavoriteButton = ({ id, isFavorite }) => {
    const [favorite, setFavorite] = useState(isFavorite)
    const { contacts, setContacts } = useContext(AppContext)

    const toggleFavorite = () => {
        const temp = [...contacts]
        temp.find(item => item.id === id).favorite = !favorite

        localStorage.setItem('contacts', JSON.stringify(temp))
        setContacts(temp)
        setFavorite(!favorite)
    }

    return (
        <FavoriteButtonStyled isFavorite={isFavorite}>
            <button type="button" onClick={() => toggleFavorite()}>
                <FavoriteIcon />
            </button>
        </FavoriteButtonStyled>
    )
}

FavoriteButton.propTypes = {
    id: PropTypes.number.isRequired,
    isFavorite: PropTypes.bool
}

FavoriteButton.defaultProps = {
    isFavorite: false
}

export default withTheme(FavoriteButton)
