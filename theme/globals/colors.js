// import { convertHexToRGB as rgba } from 'helpers/theme'

// Colors must not be used directly in components, but they should be (re)assigned here instead
const palette = {
    white: '#FFFFFF',
    grey: '#BBC4C3',
    greyLight: '#EAEAEA',
    green: '#2DA1AD',
    greenLight: '#ABD9DE',
    red: '#DE4743'
}

// Configure basic global typography colors here

export default {
    ...palette
}
