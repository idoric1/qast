const layoutTheme = {
    breakpoints: ['360px', '580px', '728px', '1024px', '1280px', '1600px'],
    variants: {
        container1260: {
            mx: 'auto',
            maxWidth: 1310
        },

        container1210: {
            mx: 'auto',
            maxWidth: 1210
        },

        container1560: {
            mx: 'auto',
            maxWidth: 1560
        },

        container1680: {
            mx: 'auto',
            maxWidth: 1680
        },

        crudContainer: {
            pl: [30, null, null, 200, 290],
            pr: 30,
            mx: ['auto'],
            maxWidth: 1190
        }
    }
}

export default layoutTheme
