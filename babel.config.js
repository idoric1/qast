/* eslint-disable import/no-commonjs */
module.exports = {
    presets: [
        'next/babel',
        [
            '@emotion/babel-preset-css-prop',
            {
                autoLabel: true,
                labelFormat: '[local]'
            }
        ]
    ],
    env: {
        production: {
            plugins: ['emotion', '@babel/plugin-proposal-export-default-from']
        },
        development: {
            plugins: [
                [
                    'emotion',
                    {
                        sourceMap: true
                    }
                ],
                '@babel/plugin-proposal-export-default-from'
            ]
        }
    }
}
